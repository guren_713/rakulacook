class RecipePagesController < ApplicationController
  def result
    @main = Dish.find(params[:main])
    @sub1 = Dish.find(params[:sub1])
    @sub2 = Dish.find(params[:sub2])
  end

  def recipe
    @recipe = JSON.parse(Dish.find(params[:id]).recipe)
    @food = JSON.parse(Dish.find(params[:id]).food)
    @favcnt = Dish.find(params[:id])
    if logged_in?
      @user = User.find(session[:user_id])
    end
  end

  def test
    @maindish = Dish.where(flag: 0).select("id")
    @subdish = Dish.where(flag: 1).select("id")
  end

  def fav
    @recipe = Dish.find(params[:id])
    @user = User.find(session[:user_id])
    @user.dishes << @recipe
#    @recipe.users << @user
  end
end
